const express = require('express');
const router = express.Router();
const config = require("../config/mysql.json");
const mysql = require('mysql');
const bodyParser = require("body-parser");
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const fs = require('fs');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

const connection = mysql.createConnection(config);
connection.connect();

router.post('/', function (request, res) {
  try {
    const password = crypto.createHash('sha1').update(request.body.password).digest('hex');

    connection.query(`SELECT id, pass FROM users WHERE users.email="${request.body.email}";`, [], function (err, results, fields) {
      console.log(results)

      if (results) {
        if (results[0].pass == password) {
          result = jwt.sign(results[0].id, fs.readFileSync('secret/master.key'));
          console.log(result)
        } else
          result = 'Unauthorized.'
      } else {
        result = 'Unauthorized.'
      }
      res.json(result);
    });
  } catch (e) {
    res.send('Unauthorized')
  }
});

module.exports = router;
