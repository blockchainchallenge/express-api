const express = require('express');
const router = express.Router();
const config = require("../config/mysql.json");
const mysql = require('mysql');
const bodyParser = require("body-parser");
const fs = require('fs');
const jwt = require('jsonwebtoken');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

const connection = mysql.createConnection(config);
connection.connect();

router.post('/', function (request, res) {
  try {
    const id = jwt.verify(request.headers.authorization, fs.readFileSync('secret/master.key'))

    var sql = "";
    if (request.body.type) sql += `type="${request.body.type}",`
    if (request.body.street) sql += `street="${request.body.street}",`
    if (request.body.number) sql += `number="${request.body.number}",`
    if (request.body.floor) sql += `floor="${request.body.floor}",`
    if (request.body.door) sql += `door="${request.body.door}",`
    if (request.body.cp) sql += `cp="${request.body.cp}",`
    if (request.body.city) sql += `city="${request.body.city}",`
    if (request.body.state) sql += `state="${request.body.state}",`

    if (sql) {
      sql = sql.substring(0, sql.length - 1)
      sql = `UPDATE family JOIN users SET ` + sql + ` WHERE (users.familyid = family.id) AND users.id="${id}";`;
      connection.query(sql, function (err, rows, fields) {
        if (err != null)
          console.log(err, rows, fields)
        res.send()
      });
    }
  } catch (e) {
    console.log(e)
    res.send("Unauthorized")
  }
});

module.exports = router;
