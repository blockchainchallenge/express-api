const express = require('express');
const router = express.Router();
const config = require("../config/mysql.json");
const mysql = require('mysql');
const fs = require('fs');
const jwt = require('jsonwebtoken');

const connection = mysql.createConnection(config);
connection.connect();

router.get('/', function (req, res, next) {
  try {
    var result = {}
    const token = req.headers.authorization.substring(7);
    const sql = `SELECT * FROM users 
              INNER JOIN accounts
              INNER JOIN family
              WHERE users.id="${jwt.verify(token, fs.readFileSync('secret/master.key'))}"
                AND users.accountid = accounts.id 
                AND users.familyid = family.id;`;

    connection.query(sql, [], function (err, results, fields) {
      result.role = results[0].role
      result.firstname = results[0].firstname
      result.lastname = results[0].lastname
      result.email = results[0].email

      result.sk = results[0].sk
      result.pk = results[0].pk

      result.type = results[0].type;
      result.street = results[0].street;
      result.number = results[0].number;
      result.floor = results[0].floor;
      result.door = results[0].door;
      result.cp = results[0].cp;
      result.city = results[0].city;
      result.state = results[0].state;
      
      console.log(result)
      res.json(result);
    });
  } catch (e) {
    console.log(e)
    res.send('Unauthorized.')
  }
});

router.get('/:email', function (req, res, next) {
  try {
    var result = {}
    const sql = `SELECT * FROM users 
              INNER JOIN accounts
              INNER JOIN family
              WHERE users.email="${req.params.email}"
                AND users.accountid = accounts.id 
                AND users.familyid = family.id;`;

    const id = jwt.verify(req.headers.authorization, fs.readFileSync('secret/master.key'));
    console.log(id)

    connection.query(sql, [], function (err, results, fields) {
      console.log(results)
      result.role = results[0].role
      result.firstname = results[0].firstname
      result.lastname = results[0].lastname
      result.email = results[0].email

      result.sk = results[0].sk
      result.pk = results[0].pk

      result.type = results[0].type;
      result.street = results[0].street;
      result.number = results[0].number;
      result.floor = results[0].floor;
      result.door = results[0].door;
      result.cp = results[0].cp;
      result.city = results[0].city;
      result.state = results[0].state;

      res.json(result);
    });
  } catch (e) {
    console.log(e)
    res.send('Unauthorized.')
  }
});

module.exports = router;

