const express = require('express');
const router = express.Router();
const config = require("../config/mysql.json");
const mysql = require('mysql');
const bodyParser = require("body-parser");
const crypto = require('crypto');
const Web3 = require('web3');
const { v4 } = require('uuid');
const web3 = new Web3();
console.log(web3.eth.accounts);

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

const connection = mysql.createConnection(config);
connection.connect();

router.post('/', function (request, res) {
  const id = v4();
  const accountid = v4();
  const familyid = v4();
  const firstname = request.body.firstname;
  const lastname = request.body.lastname;
  const email = request.body.email;
  const role = 'c';
  const password = crypto.createHash('sha1').update(request.body.password).digest('hex');
  const account = web3.eth.accounts.create();

  const sql = "INSERT INTO users (id, accountid, familyid, firstname, lastname, role, email, pass) values (\"" +
    id + "\", \"" +
    accountid + "\", \"" +
    familyid + "\", \"" +
    firstname + "\", \"" +
    lastname + "\", \"" +
    role + "\", \"" +
    email + "\", \"" +
    password + "\");" +
    "INSERT INTO accounts (id, sk, pk) values (\"" +
    accountid + "\", \"" +
    Buffer.from(account.privateKey).toString('base64') + "\", \"" +
    account.address + "\");" +
    "INSERT INTO family (id) values (\"" +
    familyid + "\");";

  connection.query(sql, function (err, rows, fields) {
    if (err != null)
      console.log(err, rows, fields)
    res.send()
  });
});

module.exports = router;
